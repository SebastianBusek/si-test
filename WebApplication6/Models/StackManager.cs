﻿using System.Collections.Generic;

namespace WebApplication6.Models
{
    public class StackManager : IStackManager
    {
        private readonly Stack<string> _stack;

        public StackManager()
        {
            _stack = new Stack<string>();
        }

        public Stack<string> Stack => _stack;

        public void Add(string value)
        {
            _stack.Push(value);
        }
    }
}