﻿namespace WebApplication6.Models
{
    public class DummyService : IDummyService
    {
        private readonly IStackManager _manager;

        public DummyService(IStackManager manager)
        {
            _manager = manager;
        }

        public void Foo()
        {
            _manager.Add("B");
        }
    }
}