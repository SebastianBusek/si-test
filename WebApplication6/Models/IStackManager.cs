﻿using System.Collections.Generic;

namespace WebApplication6.Models
{
    public interface IStackManager
    {
        Stack<string> Stack { get; }
        void Add(string value);
    }
}