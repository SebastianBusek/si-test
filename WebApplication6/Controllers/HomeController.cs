﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStackManager _manager;
        private readonly IDummyService _dummy;

        public HomeController(IStackManager manager, IDummyService dummy)
        {
            _manager = manager;
            _dummy = dummy;
        }

        public ActionResult Index()
        {
            _dummy.Foo();

            ViewBag.Stack = string.Join(" > ", _manager.Stack.Reverse());
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            _manager.Add("A");
            base.OnActionExecuting(filterContext);
        }
    }
}